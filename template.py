#!/usr/bin/env python3
import sys
import os
import pprint
import json
import time

def main():

    tmpljson = json.loads(os.environ.get('TEMPLATEJSON'))


    while True:
        print('Environment: {}\n\n'.format(json.dumps(dict(**os.environ), indent=4, sort_keys=True)))
        print('Type of tmpljson: {}\n\n'.format(type(tmpljson)))
        print('tmpljson (loaded from env var TEMPLATEJSON): {}\n\n'.format(json.dumps(tmpljson, indent=4, sort_keys=True)))
        print('tmpljson.get(\'tmpljson1\'): {}\n\n'.format(tmpljson.get('tmpljson1')))
        time.sleep(5)

if __name__ == '__main__':
    sys.exit(main())
